package tank;

public class CarroArmato {
	
	private double x = 0;
	private double y = 0;
	private double speedX = 0;
	private double speedY = 0;
	
	double getX() {
		return this.x;
	}
	
	double getY() {
		return this.y;
	}
	
	void moveRight() {
		this.speedX=4;
	}
	void moveLeft() {
		this.speedX=-4;
	}
	void moveUp() {
		this.speedY=-4;
	}
	void moveDown() {
		this.speedY=+4;
	}
	
	void stop() {
		speedX = 0;
		speedY = 0;
	}
	
	void update() {
		this.x += this.speedX;
		this.y += this.speedY;
	}
	
	void keepBetweenBorders(double ws, double hs, double wi, double hi) {
		 if (this.x + wi >= ws) {       // Exceeding right
	            this.x = ws - wi;
	        } else if (this.x < 0) {                // Exceeding left
	            this.x = 0;
	        }
	        if (this.y + hi >= hs) {      // Exceeding down
	            this.y = hs - hi;
	        } else if (this.y < 0) {                // Exceeding up
	            this.y = 0;
	        }
	}

}
