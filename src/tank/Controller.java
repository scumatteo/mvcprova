package tank;

import javafx.scene.Scene;

public class Controller {
	
	void move(Scene s, CarroArmato c) {
		s.setOnKeyPressed(e -> {
            switch (e.getCode()) {
            case UP: c.moveUp(); break;
            case DOWN: c.moveDown(); break;
            case LEFT: c.moveLeft(); break;
            case RIGHT: c.moveRight(); break;
            default: ;
            }
        });
		s.setOnKeyReleased(e -> c.stop());
	}
	
	
    
}
