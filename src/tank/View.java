package tank;

import javafx.animation.AnimationTimer;
import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;

public class View extends Application{
	
	private final int WIDTH = 600;
    private final int HEIGHT = 400;
    CarroArmato carro = new CarroArmato();
    Controller c = new Controller();
	
	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage stage) throws Exception {
		Group root = new Group();
        Scene scene = new Scene(root, WIDTH, HEIGHT);
        Rectangle r = new Rectangle(scene.getWidth()/8, scene.getHeight()/8, Color.BLACK);
        stage.setScene(scene);
        root.getChildren().add(r);
        stage.show();
        
        new AnimationTimer() {

            @Override
            public void handle(long arg0) {
            	
            	c.move(scene, carro);
            	
            	carro.update();
            	carro.keepBetweenBorders(scene.getWidth(), scene.getHeight(), r.getWidth(), r.getHeight());
            	r.setX(carro.getX());
            	r.setY(carro.getY());
            	r.setWidth(scene.getWidth()/8);
            	r.setHeight(scene.getHeight()/8);
            }
                
        }.start();;
        //animator.start();
	}


}
